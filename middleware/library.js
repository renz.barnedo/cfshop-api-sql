const moment = require('moment');

const User = require('../models/User');
const Role = require('../models/Role');
const Product = require('../models/Product');
const Image = require('../models/Image');
const Delivery = require('../models/Delivery');
const Order = require('../models/Order');
const Transaction = require('../models/Transaction');
const Contact = require('../models/Contact');

const TABLES = {
  user: User,
  role: Role,
  product: Product,
  image: Image,
  delivery: Delivery,
  order: Order,
  transaction: Transaction,
  contact: Contact,
};

exports.checkResult = (result, message = 'cannot make database changes') => {
  if (!result) {
    console.log(result);
    console.log('-----library.js->checkResult--------');
    const error = new Error();
    error.message = message;
    throw error;
  }
};

exports.addHistoryValue = (row, request) => {
  const value = {
    ...row,
    isDeleted: false,
    history: JSON.stringify([
      {
        ...row,
        method: request.method.toLowerCase(),
        createdBy: request.userId || 0,
        createdAt: moment().format(process.env.DEFAULT_DATE_FORMAT),
      },
    ]),
  };
  return value;
};

exports.findRow = async (id, table) => {
  const model = TABLES[table];
  const idColumn = `${table}Id`;
  let result = await model.findOne({
    where: {
      [idColumn]: id,
      isDeleted: 0,
    },
  });

  if (!result) {
    console.log('_____library.js->findRow', result);
    const error = new Error();
    error.message = `cannot find ${table} (${idColumn}: ${id})`;
    throw error;
  }

  result = result.dataValues;
  result.history = JSON.parse(result.history);
  return result;
};

exports.setUpdateValues = (current, updates, request) => {
  let values;
  if (current && updates) {
    values = Object.keys(updates)
      .map((key, index) => {
        let old = current[key];
        let update = updates[key];
        if (key === 'datetime') {
          old = moment(new Date(old)).format(process.env.DEFAULT_DATE_FORMAT);
          update = moment(new Date(update)).format(
            process.env.DEFAULT_DATE_FORMAT
          );
        }
        if (old != update) {
          return {
            [key]: updates[key],
          };
        }
      })
      .filter((column) => column)
      .reduce((result, current) => Object.assign(result, current), {});
  }

  const columnsWithChanges = Object.keys(values).length;
  if (!columnsWithChanges) {
    return null;
  }

  values.method = request.method.toLowerCase();
  values.createdBy = request.userId || 0;
  values.createdAt = moment().format(process.env.DEFAULT_DATE_FORMAT);

  const history = current.history;
  history.push(values);
  values.history = JSON.stringify(history);
  return values;
};

exports.setDeleteValues = (history, request) => {
  history.push({
    method: request.method.toLowerCase(),
    createdBy: request.userId || 0,
    createdAt: moment().format(process.env.DEFAULT_DATE_FORMAT),
  });
  const values = {
    isDeleted: 1,
    history: JSON.stringify(history),
  };
  return values;
};
