const Sequelize = require('sequelize');
const sequelize = require('../config/database');

module.exports = sequelize.define('users', {
  userId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  firstName: Sequelize.STRING,
  middleName: Sequelize.STRING,
  lastName: Sequelize.STRING,
  address: Sequelize.STRING,
  cellphone: Sequelize.STRING,
  telephone: Sequelize.STRING,
  isDeleted: Sequelize.BOOLEAN,
  history: Sequelize.BLOB,
});
