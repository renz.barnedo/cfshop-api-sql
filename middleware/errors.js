module.exports = (error, req, res, next) => {
  console.log(error);
  console.log('----------FROM ERROR.JS----------');
  let { status, message, data, display } = error;
  status = status || 500;
  res.status(status).json({ message, data, display });
};
