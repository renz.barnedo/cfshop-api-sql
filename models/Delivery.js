const Sequelize = require('sequelize');
const sequelize = require('../config/database');

module.exports = sequelize.define('deliveries', {
  deliveryId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  userId: Sequelize.INTEGER,
  contactId: Sequelize.INTEGER,
  status: Sequelize.TEXT,
  shippingFee: Sequelize.DECIMAL,
  courier: Sequelize.TEXT,
  instructions: Sequelize.TEXT,
  datetime: Sequelize.DATE,
  isDeleted: Sequelize.BOOLEAN,
  history: Sequelize.BLOB,
});
