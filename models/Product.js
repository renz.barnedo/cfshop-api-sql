const Sequelize = require('sequelize');
const sequelize = require('../config/database');

module.exports = sequelize.define('products', {
  productId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  name: Sequelize.STRING,
  nickname: Sequelize.STRING,
  description: Sequelize.STRING,
  price: Sequelize.DECIMAL,
  sortOrder: Sequelize.STRING,
  category: Sequelize.STRING,
  imageId: Sequelize.INTEGER,
  isDeleted: Sequelize.BOOLEAN,
  history: Sequelize.BLOB,
});
