const Sequelize = require('sequelize');
const sequelize = require('../config/database');

module.exports = sequelize.define('contacts', {
  contactId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  userId: Sequelize.INTEGER,
  contactNumbers: Sequelize.TEXT,
  receiver: Sequelize.TEXT,
  address: Sequelize.TEXT,
  isDeleted: Sequelize.BOOLEAN,
  history: Sequelize.BLOB,
});
