const express = require('express');
const router = express.Router();

const { authUser, authAdmin } = require('../middleware/authenticate');
const {
  insertContact,
  getContacts,
  updateContact,
  deleteContact,
} = require('../controllers/contact');

router.post('/contact', authUser, insertContact);
router.get('/contacts', authUser, getContacts);
router.patch('/contact', authUser, updateContact);
router.delete(
  '/contact/:contactId',
  // authUser,
  deleteContact
);

router.post('/contact/user/:userId', authAdmin, insertContact);
router.get('/contacts/user/:userId', authAdmin, getContacts);
router.patch('/contact/user/:userId', authAdmin, updateContact);
router.delete(
  '/contact/:contactId',
  // authAdmin,
  deleteContact
);

module.exports = router;
