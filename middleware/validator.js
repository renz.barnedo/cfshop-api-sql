const { body, header } = require('express-validator');

module.exports = (req, res, next) => {
  console.log('BODY:', req.body);
  console.log('----------validator.js----------');
  if (
    !Object.values(req.body).length &&
    req.method !== 'GET' &&
    req.method !== 'DELETE'
  ) {
    throw Error('no request body or params');
  }
  next();
};
