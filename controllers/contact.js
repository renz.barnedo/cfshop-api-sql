const Contact = require('../models/Contact');
const Delivery = require('../models/Delivery');
const {
  checkResult,
  addHistoryValue,
  findRow,
  setUpdateValues,
  setDeleteValues,
} = require('../middleware/library');
const moment = require('moment');
const User = require('../models/User');

exports.insertContact = async (req, res, next) => {
  try {
    let contact = req.body.contact;
    contact.userId = req.role === 'user' ? req.userId : req.params.userId;
    // find contact if it already exists
    if (contact.contactId) {
      contact.contactNumbers = JSON.stringify(contact.contactNumbers);
      const oldValues = await findRow(contact.contactId, 'contact');
      const newValues = setUpdateValues(oldValues, contact, req);
      if (!newValues) {
        next(contact);
        return;
      }
      const result = await Contact.update(newValues, {
        where: {
          contactId: contact.contactId,
          isDeleted: false,
        },
      });
      checkResult(result[0], 'cannot update contact');

      delete newValues.history;
      delete newValues.isDeleted;
      next(newValues);
      return;
    }
    contact.contactNumbers = JSON.stringify(contact.contactNumbers);
    contact = addHistoryValue(contact, req);
    contact = await Contact.create(contact);
    checkResult(contact, 'cannot create contact');
    contact = contact.dataValues;
    delete contact.isDeleted;
    delete contact.history;

    res.status(201).json({
      message: 'inserted contact',
      contact,
    });
  } catch (error) {
    next(error);
  }
};

exports.getContacts = async (req, res, next) => {
  try {
    const userId = req.role === 'user' ? req.userId : req.params.userId;
    let contacts = await Contact.findAll({
      attributes: {
        exculde: ['isDeleted', 'history'],
      },
      where: {
        userId,
        isDeleted: false,
      },
    });
    checkResult(contacts, 'cannot get contact');

    res.json({
      message: `get contact of userId: ${userId}`,
      data: contacts,
    });
  } catch (error) {
    next(error);
  }
};

exports.updateContact = async (req, res, next) => {
  try {
    let contact = req.body.contact;
    contact.contactNumbers = JSON.stringify(contact.contactNumbers);
    // TODO: find duplicate

    const oldValues = await findRow(contact.contactId, 'contact');
    const newValues = setUpdateValues(oldValues, contact, req);
    checkResult(newValues, 'no contact changes detected');

    const result = await Contact.update(newValues, {
      where: {
        contactId: contact.contactId,
        isDeleted: false,
      },
    });
    checkResult(result, 'cannot update result');

    delete newValues.isDeleted;
    delete newValues.history;
    res.status(201).json({
      message: 'updated contact',
      data: newValues,
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteContact = async (req, res, next) => {
  try {
    const contactId = req.params.contactId;
    const contact = await findRow(contactId, 'contact');
    let deleteValues = setDeleteValues(contact.history, req);
    contact.rows = await Contact.update(deleteValues, {
      where: {
        contactId,
      },
    });
    checkResult(contact.rows, 'cannot delete contact');

    delete contact.isDeleted;
    delete contact.history;
    res.status(203).json({
      message: 'deleted contact',
      contact,
    });
  } catch (error) {
    next(error);
  }
};
