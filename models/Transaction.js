const Sequelize = require('sequelize');
const sequelize = require('../config/database');

module.exports = sequelize.define('transactions', {
  transactionId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  type: Sequelize.STRING,
  item: Sequelize.STRING,
  description: Sequelize.STRING,
  price: Sequelize.DECIMAL,
  quantity: Sequelize.INTEGER,
  wallet: Sequelize.STRING,
  place: Sequelize.STRING,
  datetime: Sequelize.DATE,
  isDeleted: Sequelize.BOOLEAN,
  history: Sequelize.BLOB,
});
