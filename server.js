const express = require('express');
const morgan = require('morgan');
const compression = require('compression');
const sequelize = require('./config/database');
const multer = require('multer');
const cors = require('cors');

require('dotenv').config();

const validator = require('./middleware/validator');
const errorHandler = require('./middleware/errors');

const loginRoutes = require('./routes/login');
const userRoutes = require('./routes/user');
const transactionRoutes = require('./routes/transaction');
const tokenRoutes = require('./routes/token');
const productRoutes = require('./routes/product');
const uploadRoutes = require('./routes/upload');
const deliveryRoutes = require('./routes/delivery');
const contactRoutes = require('./routes/contact');

const app = express();
app.use(cors());
app.use(compression({ level: 9 }));
app.use(express.json());

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, process.env.PRODUCTS_FOLDER);
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === 'image/png' ||
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/jpeg'
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

app.use(
  multer({
    storage,
    fileFilter,
  }).single('image')
);

app.use(
  '/cfshop/v1/images',
  express.static(__dirname + '/' + process.env.PRODUCTS_FOLDER)
);

app.use(validator);

app.use('/cfshop/v1/auth', loginRoutes);
app.use('/cfshop/v1/person', userRoutes);
app.use('/cfshop/v1/ledger', transactionRoutes);
app.use('/cfshop/v1/token', tokenRoutes);
app.use('/cfshop/v1/item', productRoutes);
app.use('/cfshop/v1/upload', uploadRoutes);
app.use('/cfshop/v1/order', deliveryRoutes);
app.use('/cfshop/v1/delivery', contactRoutes);

app.use(errorHandler);

const PORT = process.env.PORT || 5000;
const message = `Server running on ${process.env.NODE_ENV} mode port on ${PORT}`;

sequelize
  .sync()
  .then((result) => {
    app.listen(PORT, console.log(message));
  })
  .catch((error) => {
    console.log(error);
  });
