const express = require('express');
const router = express.Router();

const {
  authAdmin,
  authUser,
  authUserOrAdmin,
} = require('../middleware/authenticate');
const { insertContact } = require('../controllers/contact');
const { insertOrder, updateOrders } = require('../controllers/order');
const {
  insertDelivery,
  getDeliveries,
  updateDelivery,
  deleteDelivery,
  getUserDeliveries,
} = require('../controllers/delivery');

router.post(
  '/delivery',
  authUserOrAdmin,
  insertDelivery,
  insertOrder
);
router.get(
  '/deliveries/user/:userId',
  // authUserOrAdmin,
  getDeliveries
);
router.get(
  '/deliveries/logged/user',
  authUser,
  getUserDeliveries
);
router.get(
  '/deliveries/status/:status',
  // authUserOrAdmin,
  getDeliveries
);
router.patch(
  '/delivery',
  authUserOrAdmin,
  updateDelivery,
  updateOrders
);
router.delete(
  '/delivery/:deliveryId',
  // authUserOrAdmin,
  deleteDelivery
);

module.exports = router;
