const Image = require('../models/Image');
const { v4: uuidv4 } = require('uuid');
const {
  checkResult,
  addHistoryValue,
  findRow,
  setUpdateValues,
} = require('../middleware/library');
const { Op } = require('sequelize');

exports.insertImage = async (req, res, next) => {
  if (!req.body.image) {
    checkResult(!req.body.image, 'cannot find image');
    return;
  }
  try {
    let image = req.body.image;
    const duplicate = await Image.findOne({
      where: {
        placeholder: image.placeholder,
        category: image.category,
        description: image.description,
        isDeleted: false,
      },
    });
    checkResult(!duplicate, 'duplicate image category and description');
    image = addHistoryValue(image, req);
    image = {
      ...image,
      name: uuidv4() + image.name,
    };
    next(image);
  } catch (error) {
    next(error);
  }
};

exports.updateImage = async (req, res, next) => {
  if (!req.body.image || !req.body.image.imageId) {
    next({});
    return;
  }
  try {
    let image = req.body.image;
    const duplicate = await Image.findOne({
      where: {
        placeholder: image.placeholder,
        isDeleted: false,
        [Op.not]: {
          imageId: image.imageId,
        },
      },
    });
    checkResult(!duplicate, 'duplicate image category and description');

    if (image.name) {
      image.name = uuidv4() + image.name;
    }

    const current = await findRow(image.imageId, 'image');
    const changes = setUpdateValues(current, image, req);
    // checkResult(changes, 'no changes found');
    if (!changes) {
      next({});
    }

    result = await Image.update(changes, {
      where: {
        imageId: image.imageId,
        isDeleted: false,
      },
    });
    checkResult(result, 'cannot update image');

    image = {
      ...image,
      rows: result[0],
    };
    next(image);
  } catch (error) {
    next(error);
  }
};
