const express = require('express');
const router = express.Router();

const { authAdmin } = require('../middleware/authenticate');
const {
  insertTransaction,
  selectTransactions,
  selectTypeTransactions,
  updateTransaction,
  deleteTransaction,
} = require('../controllers/transaction');

router.post('/transaction', authAdmin, insertTransaction);

router.get('/transactions', authAdmin, selectTransactions);

router.get('/transaction/type/:type', authAdmin, selectTypeTransactions);

router.patch('/transaction', authAdmin, updateTransaction);

router.delete('/transaction/:transactionId', authAdmin, deleteTransaction);

module.exports = router;
