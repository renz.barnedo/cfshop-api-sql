const Order = require('../models/Order');
// const Action = require('../models/Action');
const Product = require('../models/Product');
const { Op } = require('sequelize');
const sequelize = require('sequelize');
const {
  checkResult,
  addHistoryValue,
  findRow,
  setUpdateValues,
} = require('../middleware/library');

exports.insertOrder = async (created, req, res, next) => {
  const { contact, delivery } = created;
  if (!req.body.orders || !req.body.orders.length) {
    next();
    return;
  }
  try {
    let orders = (
      await Promise.all(
        req.body.orders.map(async (order) => {
          const duplicate = await Order.findOne({
            where: {
              productId: order.productId,
              quantity: order.quantity,
              isDeleted: false,
            },
          });
          if (duplicate) {
            return null;
          }
          order.deliveryId = delivery.deliveryId;
          order = addHistoryValue(order, req);
          return order;
        })
      )
    ).filter((order) => order);

    orders = await Order.bulkCreate(orders);
    checkResult(orders, 'cannot insert orders');

    orders.forEach((order) => {
      delete order.dataValues.isDeleted;
      delete order.dataValues.history;
      return order;
    });

    res.status(201).json({
      message: 'delivery and orders created',
      data: {
        contact,
        delivery,
        orders,
      },
    });
  } catch (error) {
    next(error);
  }
};

// TODO: delete orders, insert order
exports.updateOrders = async (newDelivery, req, res, next) => {
  if (newDelivery.message) {
    next(newDelivery);
  }
  if (!req.body.orders || !req.body.orders.length) {
    res.status(201).json({
      message: `${newDelivery.status} order`,
      data: newDelivery,
    });
  }
  try {
    const deliveryId = req.body.delivery.deliveryId;
    const orders = req.body.orders;
    const data = {
      delivery: newDelivery,
      orders: {
        rows: orders.length,
        updated: [],
        created: [],
      },
    };

    await Order.update(
      {
        isDeleted: true,
      },
      {
        where: {
          deliveryId,
        },
      }
    );

    await Promise.all(
      orders.map(async (order) => {
        if (order.orderId) {
          await Order.update(
            {
              isDeleted: false,
            },
            {
              where: {
                orderId: order.orderId,
              },
            }
          );
          const oldValues = await findRow(order.orderId, 'order');
          const newValues = setUpdateValues(oldValues, order, req);
          checkResult(newValues, 'no changes found');
          const result = await Order.update(newValues, {
            where: {
              orderId: order.orderId,
            },
          });
          checkResult(result, 'cannot update order');
          delete newValues.isDeleted;
          delete newValues.history;
          data.orders.updated.push(newValues);
          return order;
        }
        order.deliveryId = deliveryId;
        order = addHistoryValue(order, req);
        await Order.create(order);

        delete order.isDeleted;
        delete order.history;
        data.orders.created.push(order);
        return order;
      })
    );

    res.status(202).json({
      message: 'updated delivery and its orders',
      data,
    });
  } catch (error) {}
};
