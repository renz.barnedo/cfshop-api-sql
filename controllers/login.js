const bcrypt = require('bcryptjs');
const Role = require('../models/Role');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const moment = require('moment');

const throwError = (message, status) => {
  const error = new Error(message);
  error.status = status;
  throw error;
};

const SALT_ROUNDS = 12;

exports.fbLogin = async (req, res, next) => {
  if (!req.body) {
    res.status(500).json({
      message: 'cannot log in',
    });
  }
  try {
    const account = req.body;
    const role = await Role.findOne({
      where: {
        username: account.id,
      },
    });
    if (!role) {
      const { firstName, lastName } = account;
      let user = {
        firstName,
        lastName,
      };
      user = {
        ...user,
        isDeleted: false,
        history: JSON.stringify([
          {
            ...user,
            method: account.provider,
            createdBy: account.id,
            createdAt: moment().format(process.env.DEFAULT_DATE_FORMAT),
          },
        ]),
      };
      user = await User.create(user);

      let role = {
        userId: user.dataValues.userId,
        email: account.email,
        username: account.id,
        password: bcrypt.hashSync(process.env.TOKEN, SALT_ROUNDS),
        role: 'user',
      };
      role = {
        ...role,
        isDeleted: false,
        history: JSON.stringify([
          {
            ...role,
            method: account.provider,
            createdBy: account.id,
            createdAt: moment().format(process.env.DEFAULT_DATE_FORMAT),
          },
        ]),
      };
      role = await Role.create(role);
      const token = jwt.sign(
        {
          userId: role.dataValues.userId,
          role: role.dataValues.role,
        },
        process.env.TOKEN,
        { expiresIn: '1h' }
      );
      res.json({
        message: 'fb login success',
        token: {
          user: token,
          auth: req.body.authToken,
        },
      });
      return;
    }

    res.json({
      message: 'current fb login success',
      token: {
        user: jwt.sign(
          {
            userId: role.dataValues.userId,
            role: role.dataValues.role,
          },
          process.env.TOKEN,
          { expiresIn: '1h' }
        ),
        auth: req.body.authToken,
      },
    });
  } catch (error) {
    next(error);
  }
};

exports.login = async (req, res, next) => {
  try {
    const { username, password } = req.body;

    const user = await Role.findOne({
      where: {
        username,
      },
    });

    if (!user) {
      throwError('user not found', 404);
    }

    const passwordsMatched = bcrypt.compareSync(password, user.password);

    if (!passwordsMatched) {
      throwError('wrong password', 400);
    }

    const token = jwt.sign(
      {
        userId: user.userId,
        role: user.role,
      },
      process.env.TOKEN,
      { expiresIn: '30m' }
    );

    // TODO: update token in role

    res.status(200).json({
      message: 'login success',
      data: {
        token,
      },
    });
  } catch (error) {
    next(error);
  }
};

exports.logout = async (req, res, next) => {
  try {
    const role = await Role.update(
      { token: null },
      {
        where: {
          token: req.token,
        },
      }
    );

    res.status(200).json({
      message: 'logged out',
    });
  } catch (error) {
    next(error);
  }
};
