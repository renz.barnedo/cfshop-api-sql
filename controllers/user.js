const User = require('../models/User');
const Role = require('../models/Role');
const {
  checkResult,
  addHistoryValue,
  findRow,
  setUpdateValues,
  setDeleteValues,
} = require('../middleware/library');
const { Op } = require('sequelize');

exports.insertUser = async (req, res, next) => {
  try {
    let user = req.body.user;
    const duplicate = await User.findOne({
      where: {
        firstName: user.firstName,
        middleName: user.middleName || '',
        lastName: user.lastName,
        address: user.address,
        isDeleted: false,
      },
    });
    checkResult(!duplicate, 'duplicate user name and address');
    user = addHistoryValue(user, req);
    next(user);
  } catch (error) {
    next(error);
  }
};

exports.selectUsers = async (req, res, next) => {
  try {
    let condition = {
      attributes: {
        exclude: ['isDeleted', 'history'],
      },
      include: {
        association: Role.belongsTo(User, { foreignKey: 'userId' }),
        attributes: {
          exclude: ['isDeleted', 'history'],
        },
        where: {
          role: 'user',
          isDeleted: false,
        },
      },
      where: {
        isDeleted: false,
      },
    };

    const users = await User.findAll(condition);
    checkResult(users, 'cannot select user');
    users.forEach((user) => {
      const column = user.dataValues;
      Object.keys(column).forEach((key) => {
        user[key] = user[key] || '';
      });
      column.fullName = `${column.firstName} ${column.middleName} ${column.lastName}`;
    });

    res.json({
      message: 'selected all users',
      data: users,
    });
  } catch (error) {
    next(error);
  }
};

exports.updateUser = async (req, res, next) => {
  if (!req.body.user || !req.body.user.userId) {
    next(null);
    return;
  }
  try {
    const user = req.body.user;
    const duplicate = await User.findOne({
      where: {
        firstName: user.firstName,
        middleName: user.middleName || '',
        lastName: user.lastName,
        address: user.address,
        isDeleted: false,
        [Op.not]: {
          userId: user.userId,
        },
      },
    });
    checkResult(!duplicate, "duplicate user's name and address");
    const oldValues = await findRow(user.userId, 'user');
    const newValues = setUpdateValues(oldValues, user, req);
    if (!newValues) {
      next({});
      return;
    }
    // checkResult(newValues, 'no changes found');
    newValues.rows = (
      await User.update(newValues, {
        where: {
          userId: user.userId,
        },
      })
    )[0];
    checkResult(newValues.rows, 'cannot update user');
    delete newValues.isDeleted;
    delete newValues.history;
    next(newValues);
  } catch (error) {
    next(error);
  }
};

exports.deleteUser = async (req, res, next) => {
  try {
    const userId = req.params.userId;
    const user = await findRow(userId, 'user');
    let values = setDeleteValues(user.history, req);
    const condition = {
      where: {
        userId,
      },
    };

    user.rows = (await User.update(values, condition))[0];
    checkResult(user.rows, 'cannot delete user');

    const role = await findRow(userId, 'role');
    values = setDeleteValues(role.history, req);
    role.rows = (await Role.update(values, condition))[0];
    checkResult(role.rows, 'cannot delete role');

    delete user.history;
    delete role.history;
    res.status(203).json({
      message: 'deleted user and its role',
      data: { user, role },
    });
  } catch (error) {
    next(error);
  }
};
