const express = require('express');
const router = express.Router();

const { authAdmin } = require('../middleware/authenticate');
const { insertRole, updateRole } = require('../controllers/role');
const {
  insertUser,
  selectUsers,
  updateUser,
  deleteUser,
} = require('../controllers/user');

router.post('/user', authAdmin, insertUser, insertRole);

router.get('/users', authAdmin, selectUsers);

router.patch('/user', authAdmin, updateUser, updateRole);

router.delete('/user/:userId', authAdmin, deleteUser);

module.exports = router;
