const throwError = (message, status) => {
  const error = new Error(message);
  error.status = status;
  throw error;
};

exports.uploadImage = (req, res, next) => {
  const image = req.file;
  if (!image) {
    throwError('cannot upload image', 401);
  }
  res.status(200).json({
    message: 'image uploaded',
    image,
  });
};
