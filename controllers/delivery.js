const Delivery = require('../models/Delivery');
const Order = require('../models/Order');
const Product = require('../models/Product');
const {
  checkResult,
  addHistoryValue,
  findRow,
  setUpdateValues,
  setDeleteValues,
} = require('../middleware/library');
const moment = require('moment');
const User = require('../models/User');
const Role = require('../models/Role');
const Image = require('../models/Image');
const Contact = require('../models/Contact');

exports.insertDelivery = async (req, res, next) => {
  try {
    let delivery = req.body.delivery;
    let contact = req.body.contact;
    // const duplicate = await Delivery.findOne({
    //   where: {
    //     userId: delivery.userId,
    //     contactNumbers: delivery.contactNumbers,
    //     address: delivery.address,
    //     status: delivery.status,
    //     isDeleted: false,
    //   },
    // });
    // checkResult(!duplicate, 'duplicate userId, contact and status');
    delivery.contactId = contact.contactId;
    delivery.datetime = moment().format(process.env.DEFAULT_DATE_FORMAT);
    delivery = addHistoryValue(delivery, req);
    delivery = await Delivery.create(delivery);
    checkResult(delivery, 'cannot create delivery');
    delivery = delivery.dataValues;

    delete delivery.isDeleted;
    delete delivery.history;
    next({ contact, delivery });
  } catch (error) {
    next(error);
  }
};

exports.getLatestDelivery = async (req, res, next) => {
  try {
    const userId = req.params.userId;
    const delivery = await Delivery.findAll({
      limit: 1,
      where: {
        userId,
      },
      order: [['deliveryId', 'DESC']],
    });

    res.json({
      message: 'get delivery',
      delivery,
    });
  } catch (error) {
    next(error);
  }
};

exports.getUserDeliveries = async (req, res, next) => {
  try {
    const userId = req.userId;
    const condition = {
      attributes: {
        exclude: ['isDeleted', 'history'],
      },
      include: [
        {
          association: Delivery.belongsTo(User, { foreignKey: 'userId' }),
          attributes: {
            exclude: ['isDeleted', 'history'],
          },
          where: {
            isDeleted: false,
          },
          include: {
            association: User.hasOne(Role, { foreignKey: 'userId' }),
            attributes: ['email', 'username'],
            where: {
              isDeleted: false,
            },
          },
        },
        {
          association: Delivery.hasMany(Order, {
            foreignKey: 'deliveryId',
          }),
          attributes: {
            exclude: ['isDeleted', 'history'],
          },
          where: {
            isDeleted: false,
          },
          include: {
            association: Order.belongsTo(Product, { foreignKey: 'productId' }),
            attributes: {
              exclude: [
                'productId',
                'nickname',
                'imageId',
                'isDeleted',
                'history',
              ],
            },
            where: {
              isDeleted: false,
            },
            include: {
              association: Product.hasOne(Image, { foreignKey: 'imageId' }),
              attributes: ['name', 'placeholder'],
              where: {
                isDeleted: false,
              },
            },
          },
        },
      ],
      where: {
        isDeleted: false,
        userId,
      },
      order: [['datetime', 'DESC']],
    };

    const deliveries = await Delivery.findAll(condition);
    deliveries.forEach((delivery) => {
      delivery = delivery.dataValues;
      const user = delivery.user.dataValues;
      user.fullName = `${user.firstName} ${user.middleName || ''} ${
        user.lastName
      }`;
      delivery.datetime = moment(delivery.datetime).format(
        process.env.DEFAULT_DATE_FORMAT
      );
      delivery.total = 0;
      delivery.orders.forEach((order) => {
        const product = order.product.dataValues;
        delivery.total =
          delivery.total + parseFloat(product.price) * order.quantity;
        const image = product.image.dataValues;
        image.path = process.env.PHOTO_PATH + image.name;
      });
    });
    checkResult(deliveries, 'cannot select delivery');

    res.json({
      message: `selected all deliveries of userId ${userId}`,
      data: deliveries,
    });
  } catch (error) {}
};

exports.getDeliveries = async (req, res, next) => {
  try {
    const column = Object.keys(req.params)[0];
    let value = req.params[column];
    value = isNaN(value) ? value.replace(/-/g, ' ') : parseInt(value);
    const condition = {
      attributes: {
        exclude: ['isDeleted', 'history'],
      },
      include: [
        {
          association: Delivery.belongsTo(User, { foreignKey: 'userId' }),
          attributes: {
            exclude: ['isDeleted', 'history'],
          },
          where: {
            isDeleted: false,
          },
          include: {
            association: User.hasOne(Role, { foreignKey: 'userId' }),
            attributes: ['email', 'username'],
            where: {
              isDeleted: false,
            },
          },
        },
        {
          association: Delivery.hasMany(Order, {
            foreignKey: 'deliveryId',
          }),
          attributes: {
            exclude: ['isDeleted', 'history'],
          },
          where: {
            isDeleted: false,
          },
          include: {
            association: Order.belongsTo(Product, { foreignKey: 'productId' }),
            attributes: {
              exclude: [
                'productId',
                'nickname',
                'imageId',
                'isDeleted',
                'history',
              ],
            },
            where: {
              isDeleted: false,
            },
          },
        },
        {
          association: User.hasOne(Contact, { foreignKey: 'userId' }),
          attributes: ['contactNumbers', 'receiver', 'address'],
          where: {
            isDeleted: false,
          },
        },
        // {
        //   association: Delivery.belongsTo(Contact, { foreignKey: 'contactId' }),
        //   attributes: ['contactNumbers', 'receiver', 'address'],
        //   where: {
        //     isDeleted: false,
        //   },
        // },
      ],
      where: {
        isDeleted: false,
        [column]: value,
      },
      order: [['datetime', 'DESC']],
    };

    const deliveries = await Delivery.findAll(condition);
    checkResult(deliveries, 'cannot select delivery');
    deliveries.forEach((delivery) => {
      delivery = delivery.dataValues;
      const user = delivery.user.dataValues;
      user.fullName = `${user.firstName} ${user.middleName || ''} ${
        user.lastName
      }`;
      delivery.datetime = moment(delivery.datetime).format(
        process.env.DEFAULT_DATE_FORMAT
      );
      const contact = delivery.contact.dataValues;
      contact.contactNumbers = JSON.parse(contact.contactNumbers);
      delivery.total = 0;
      delivery.orders.forEach((order) => {
        const product = order.product.dataValues;
        delivery.total =
          delivery.total + parseFloat(product.price) * order.quantity;
      });
    });

    res.json({
      message: 'selected all deliveries',
      [column]: value,
      data: deliveries,
    });
  } catch (error) {
    next(error);
  }
};

exports.updateDelivery = async (req, res, next) => {
  try {
    let delivery = req.body.delivery;
    if (delivery.status === 'Canceled') {
      const deliveryTime = (await Delivery.findByPk(delivery.deliveryId))
        .datetime;
      const cancellableTime = moment(deliveryTime).add(10, 'minutes');
      const now = moment();
      if (now > cancellableTime && req.role === 'user') {
        checkResult(false, 'too late to cancel');
      }
    }
    // const duplicate = await Delivery.findOne({
    //   where: {
    //     userId: delivery.userId,
    //     isDeleted: false,
    //   },
    // });
    // checkResult(!duplicate, 'duplicate image category and description');
    if (delivery.contactNumbers) {
      delivery.contactNumbers = JSON.stringify(delivery.contactNumbers);
    }

    const oldValues = await findRow(delivery.deliveryId, 'delivery');
    const newValues = setUpdateValues(oldValues, delivery, req);
    // checkResult(newValues, 'no changes found');
    if (!newValues) {
      next(null);
      return;
    }

    newValues.shippingFee = parseFloat(newValues.shippingFee);
    const result = await Delivery.update(newValues, {
      where: {
        deliveryId: delivery.deliveryId,
        isDeleted: false,
      },
    });
    checkResult(result, 'cannot update delivery');

    delete newValues.history;
    delivery = {
      ...newValues,
      rows: result[0],
    };
    next(delivery);
  } catch (error) {
    next(error);
  }
};

exports.deleteDelivery = async (req, res, next) => {
  try {
    const deliveryId = req.params.deliveryId;
    const delivery = await findRow(deliveryId, 'delivery');
    let deleteValues = setDeleteValues(delivery.history, req);
    delivery.rows = (
      await Delivery.update(deleteValues, {
        where: {
          deliveryId,
        },
      })
    )[0];
    checkResult(delivery.rows, 'cannot delete product');

    let orders = await Order.findAll({
      where: {
        deliveryId,
        isDeleted: 0,
      },
    });
    orders = await Promise.all(
      orders.map(async (order) => {
        order = order.dataValues;
        order.history = JSON.parse(order.history);
        let deleteValues = setDeleteValues(order.history, req);
        order.rows = await Order.update(deleteValues, {
          where: {
            orderId: order.orderId,
          },
        });
        delete order.isDeleted;
        delete order.history;
        return order;
      })
    );

    delete delivery.isDeleted;
    delete delivery.history;
    res.status(203).json({
      message: 'delivery and its orders deleted',
      data: {
        delivery,
        orders,
      },
    });
  } catch (error) {
    next(error);
  }
};
