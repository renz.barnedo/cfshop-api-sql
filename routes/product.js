const express = require('express');
const router = express.Router();

const { authAdmin } = require('../middleware/authenticate');
const { insertImage, updateImage } = require('../controllers/image');
const {
  insertProduct,
  selectProductByName,
  selectProducts,
  updateProduct,
  deleteProduct,
} = require('../controllers/product');

router.post('/product', authAdmin, insertImage, insertProduct);

router.get('/product/:name', selectProductByName);
router.get('/products', selectProducts);

router.patch('/product', authAdmin, updateImage, updateProduct);

router.delete('/product/:productId', authAdmin, deleteProduct);

module.exports = router;
