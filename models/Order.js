const Sequelize = require('sequelize');
const sequelize = require('../config/database');

module.exports = sequelize.define('orders', {
  orderId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  deliveryId: Sequelize.INTEGER,
  productId: Sequelize.INTEGER,
  quantity: Sequelize.INTEGER,
  isDeleted: Sequelize.BOOLEAN,
  history: Sequelize.BLOB,
});
