const User = require('../models/User');
const Role = require('../models/Role');
const {
  checkResult,
  addHistoryValue,
  findRow,
  setUpdateValues,
} = require('../middleware/library');

const bcrypt = require('bcryptjs');
const { Op } = require('sequelize');

const SALT_ROUNDS = 12;

exports.insertRole = async (user, req, res, next) => {
  if (user.message || !req.body.role) {
    next(user);
    return;
  }
  try {
    let role = req.body.role;
    const duplicate = await Role.findOne({
      where: {
        isDeleted: 0,
        [Op.or]: {
          email: role.email || '',
          username: role.username || '',
        },
      },
    });
    checkResult(!duplicate, 'duplicate email or username');

    user = await User.create(user);
    checkResult(user, 'cannot create user');

    const temp = (user.firstName[0] + user.lastName).toLowerCase();
    role.username = temp;
    role.password = bcrypt.hashSync(temp, SALT_ROUNDS);
    role.userId = user.userId;
    role = addHistoryValue(role, req);

    role = await Role.create(role);
    checkResult(role, 'cannot insert role');

    delete user.dataValues.isDeleted;
    delete user.dataValues.history;
    delete role.dataValues.isDeleted;
    delete role.dataValues.history;
    res.status(201).json({
      message: 'user and role created',
      data: { user, role },
    });
  } catch (error) {
    next(error);
  }
};

exports.updateRole = async (user, req, res, next) => {
  if (user.message) {
    next(user);
    return;
  }
  try {
    const role = req.body.role;
    const duplicate = await Role.findOne({
      where: {
        isDeleted: 0,
        [Op.or]: {
          email: role.email || '',
          username: role.username || '',
        },
        [Op.not]: {
          roleId: role.roleId,
        },
      },
    });
    checkResult(!duplicate, 'duplicate email or username');

    if (role.password) {
      role.password = bcrypt.hashSync(role.password, SALT_ROUNDS);
    }
    const oldValues = await findRow(role.roleId, 'role');
    const newValues = setUpdateValues(oldValues, role, req);
    // checkResult(newValues, 'no changes found');

    if (newValues) {
      newValues.rows = (
        await Role.update(newValues, {
          where: {
            roleId: role.roleId,
          },
        })
      )[0];
      checkResult(newValues.rows, 'cannot update role');
      delete newValues.isDeleted;
      delete newValues.history;
    }

    res.status(203).json({
      message: `user and role updated`,
      data: {
        user,
        role: newValues,
      },
    });
  } catch (error) {
    next(error);
  }
};
