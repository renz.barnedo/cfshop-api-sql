const jwt = require('jsonwebtoken');

exports.verifyToken = (req, res, next) => {
  try {
    const authorization = req.headers.authorization;
    const token = authorization.split(' ')[1];
    var decoded = jwt.verify(token, process.env.TOKEN);

    if (!decoded) {
      const error = new Error();
      error.status = 401;
      error.message = 'cannot verify token';
      throw error;
    }

    res.status(200).json({
      message: 'token verified',
    });
  } catch (err) {
    next(err);
  }
};
