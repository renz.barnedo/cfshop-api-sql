const Sequelize = require('sequelize');
const sequelize = require('../config/database');

module.exports = sequelize.define('images', {
  imageId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  placeholder: Sequelize.STRING,
  name: Sequelize.STRING,
  category: Sequelize.STRING,
  description: Sequelize.STRING,
  isDeleted: Sequelize.BOOLEAN,
  history: Sequelize.BLOB,
});
