exports.responseMessage = async (req, res, next) => {
  const { status, message, data } = req.response;
  res.status(status || 200).json({
    message,
    data,
  });
};
