const Sequelize = require('sequelize');
const sequelize = require('../config/database');

module.exports = sequelize.define('roles', {
  roleId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  userId: Sequelize.INTEGER,
  email: Sequelize.STRING,
  username: Sequelize.STRING,
  password: Sequelize.STRING,
  role: Sequelize.STRING,
  rankId: Sequelize.INTEGER,
  loginToken: Sequelize.STRING,
  isDeleted: Sequelize.BOOLEAN,
  history: Sequelize.BLOB,
});
