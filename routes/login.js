const express = require('express');
const router = express.Router();

const { login, fbLogin } = require('../controllers/login');

router.post('/login', login);
router.post('/loginfb', fbLogin);

module.exports = router;
