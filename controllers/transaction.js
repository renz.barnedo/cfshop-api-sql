const Transaction = require('../models/Transaction');
const {
  checkResult,
  addHistoryValue,
  setDeleteValues,
  findRow,
  setUpdateValues,
} = require('../middleware/library');

exports.insertTransaction = async (req, res, next) => {
  try {
    let transaction = req.body.transaction;

    const duplicate = await Transaction.findOne({
      where: {
        type: transaction.type,
        item: transaction.item,
        description: transaction.description,
        price: transaction.price,
        quantity: transaction.quantity,
        datetime: transaction.datetime,
        isDeleted: false,
      },
    });
    checkResult(!duplicate, 'duplicate transaction');

    transaction = addHistoryValue(transaction, req);
    transaction = await Transaction.create(transaction);
    checkResult(transaction, 'cannot create transaction');

    delete transaction.dataValues.isDeleted;
    delete transaction.dataValues.history;
    res.status(201).json({
      message: 'transaction inserted',
      data: transaction,
    });
  } catch (error) {
    next(error);
  }
};

exports.selectTransactions = async (req, res, next) => {
  try {
    const transactions = await Transaction.findAll({
      attributes: {
        exclude: ['isDeleted', 'history'],
      },
      where: {
        isDeleted: 0,
      },
    });

    res.json({
      message: 'selected all transactions',
      data: transactions,
    });
  } catch (error) {
    next(error);
  }
};

exports.selectTypeTransactions = async (req, res, next) => {
  try {
    const type = req.params.type;
    const transactions = await Transaction.findAll({
      attributes: {
        exclude: ['isDeleted', 'history'],
      },
      where: {
        isDeleted: 0,
        type,
      },
    });

    res.json({
      message: 'selected all transactions',
      data: transactions,
    });
  } catch (error) {
    next(error);
  }
};

exports.updateTransaction = async (req, res, next) => {
  try {
    const updates = req.body.transaction;
    const current = await findRow(updates.transactionId, 'transaction');
    const changes = setUpdateValues(current, updates, req);
    checkResult(changes, 'no changes found');

    changes.rows = (
      await Transaction.update(changes, {
        where: {
          transactionId: updates.transactionId,
        },
      })
    )[0];
    checkResult(changes.rows, 'cannot update transaction');

    delete changes.history;
    res.status(203).json({
      message: 'updated transaction',
      data: changes,
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteTransaction = async (req, res, next) => {
  try {
    const transactionId = req.params.transactionId;
    const current = await findRow(transactionId, 'transaction');
    const changes = setDeleteValues(current.history, req);
    const condition = {
      where: {
        transactionId,
      },
    };
    current.rows = (await Transaction.update(changes, condition))[0];
    checkResult(current.rows, 'cannot delete transaction');
    delete current.isDeleted;
    delete current.history;
    res.status(203).json({
      message: 'deleted transaction',
      data: current,
    });
  } catch (error) {
    next(error);
  }
};
