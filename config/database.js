const Sequelize = require('sequelize');
require('dotenv').config();

const sequelize = new Sequelize(
  process.env.SCHEMA,
  process.env.USER,
  process.env.PASSWORD,
  {
    dialect: 'mysql',
    host: process.env.HOST,
    // timezone: process.env.TIMEZONE,
    useUTC: false,
    define: {
      timestamps: false,
    },
  }
);
module.exports = sequelize;
