const Product = require('../models/Product');
const Image = require('../models/Image');
const {
  checkResult,
  addHistoryValue,
  findRow,
  setUpdateValues,
  setDeleteValues,
} = require('../middleware/library');
const { Op } = require('sequelize');
const sequelize = require('sequelize');

exports.insertProduct = async (image, req, res, next) => {
  if (image.message) {
    next(image);
    return;
  }
  try {
    let product = req.body.product;
    const duplicate = await Product.findOne({
      where: {
        name: product.name,
        isDeleted: 0,
      },
    });
    checkResult(!duplicate, 'duplicate product name');

    image = addHistoryValue(image, req);
    image = await Image.create(image);
    checkResult(image, 'cannot insert image');
    image = image.dataValues;
    delete image.isDeleted;
    delete image.history;

    product = {
      ...product,
      imageId: image.imageId,
    };
    product = addHistoryValue(product, req);
    product = await Product.create(product);
    checkResult(product, 'cannot create product');
    product = product.dataValues;

    await Product.update(
      {
        sortOrder: product.productId,
      },
      {
        where: {
          productId: product.productId,
        },
      }
    );

    delete product.isDeleted;
    delete product.history;
    res.status(201).json({
      message: 'product inserted',
      data: {
        product,
        image,
      },
    });
  } catch (error) {
    next(error);
  }
};

exports.selectProducts = async (req, res, next) => {
  try {
    const condition = {
      attributes: {
        exclude: ['nickname', 'isDeleted', 'history'],
      },
      include: {
        association: Product.hasOne(Image, { foreignKey: 'imageId' }),
        attributes: {
          exclude: ['imageId', 'description', 'isDeleted', 'history'],
        },
      },
      where: {
        isDeleted: 0,
      },
      order: sequelize.col('sortOrder'),
    };

    const products = await Product.findAll(condition);
    checkResult(products, 'cannot find products');

    products.forEach((product) => {
      const column = product.dataValues;
      const image = column.image.dataValues;
      image.path = process.env.PHOTO_PATH + image.name;
      delete column.isDeleted;
      delete column.history;
    });

    res.json({
      message: 'products selected',
      data: products,
    });
  } catch (error) {
    next(error);
  }
};

exports.selectProductByName = async (req, res, next) => {
  try {
    let name = req.params.name;
    name = name.includes('-') ? name.replace(/-/g, ' ') : name;

    const condition = {
      attributes: {
        exclude: ['sortOrder', 'nickname', 'isDeleted', 'history'],
      },
      include: {
        association: Product.hasOne(Image, { foreignKey: 'imageId' }),
        attributes: {
          exclude: ['imageId', 'description', 'isDeleted', 'history'],
        },
      },
      where: {
        isDeleted: 0,
        name: name,
      },
    };

    const product = await Product.findOne(condition);
    checkResult(product, 'cannot find product');
    const image = product.image.dataValues;
    image.path = process.env.PHOTO_PATH + image.name;

    delete product.dataValues.isDeleted;
    delete product.dataValues.history;
    res.json({
      message: 'selected product',
      data: product,
    });
  } catch (error) {
    next(error);
  }
};

exports.updateProduct = async (image, req, res, next) => {
  try {
    let newProduct = req.body.product;
    const oldValues = await findRow(newProduct.productId, 'product');
    const newValues = setUpdateValues(oldValues, newProduct, req);
    checkResult(newValues, 'no changes found');

    if (newProduct.name) {
      const duplicate = await Product.findOne({
        where: {
          isDeleted: 0,
          name: newProduct.name,
          [Op.not]: {
            productId: newProduct.productId,
          },
        },
      });
      checkResult(!duplicate, 'duplicate product name');
    }

    const result = await Product.update(newValues, {
      where: {
        productId: newProduct.productId,
      },
    });
    checkResult(result, 'cannot update product');

    delete newValues.history;
    res.status(203).json({
      message: 'updated product and its image',
      data: {
        product: newValues,
        image,
      },
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteProduct = async (req, res, next) => {
  if (!req.params.productId) {
    checkResult(!req.body.image, 'cannot find product id');
    return;
  }
  try {
    const productId = req.params.productId;
    const product = await findRow(productId, 'product');
    let deleteValues = setDeleteValues(product.history, req);
    const condition = {
      where: {
        productId,
      },
    };
    product.rows = (await Product.update(deleteValues, condition))[0];
    checkResult(product.rows, 'cannot delete product');

    const image = await findRow(productId, 'image');
    deleteValues = setDeleteValues(image.history, req);
    condition.where = { imageId: productId };
    image.rows = (await Image.update(deleteValues, condition))[0];
    checkResult(product.rows, 'cannot delete product');

    delete product.history;
    delete image.history;
    res.status(203).json({
      message: 'deleted product and its image',
      data: { product, image },
    });
  } catch (error) {
    next(error);
  }
};
